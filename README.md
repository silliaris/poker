# Poker

![](pokerWin.png)


This is Poker game that works in console.

My original project for learning C/C++. Basic work with pointers, structs, and general coding standards.

![](poker.png)
Screenshot from Manjaro Linux

---

### Features
- Console UI
- Basic AI to play against
- Controlling game via keyboard commands
