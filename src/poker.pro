TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    betting.cpp \
    cardCalculation.cpp \
    initGame.cpp \
    poker.cpp \
    UI.cpp

SUBDIRS += \
    poker.pro

DISTFILES += \
    poker.pro.user

HEADERS += \
    betting.hpp \
    cardCalculation.hpp \
    initGame.hpp \
    poker.hpp \
    UI.hpp
